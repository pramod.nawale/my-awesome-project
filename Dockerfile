FROM 		tomcat:8.0.21-jre8

MAINTAINER      Pramod nawale	

COPY 		./software/ /usr/local/tomcat/webapps/

EXPOSE 8181
